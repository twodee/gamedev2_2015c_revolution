﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
  public float speed;
  public Renderer backgroundRenderer;

  private new Rigidbody2D rigidbody;
  private Vector2 baseOffset;
  private bool isFacingRight = true;

  void Start() {
    rigidbody = GetComponent<Rigidbody2D>(); 
    baseOffset = backgroundRenderer.sharedMaterial.GetTextureOffset("_MainTex");
  }

  void Update() {
  }
  
  void FixedUpdate() {
    // Keep on screen.
    rigidbody.isKinematic = true;
    Vector3 position = transform.position;
    float halfHeight = GetComponent<BoxCollider2D>().bounds.extents.y;
    position.y = Mathf.Clamp(position.y, -Camera.main.orthographicSize + halfHeight, Camera.main.orthographicSize - halfHeight);
    transform.position = position;
    rigidbody.isKinematic = false;

    // Set velocity.
    float horizontal = Input.GetAxis("Horizontal"); 
    float vertical = Input.GetAxis("Vertical"); 
    rigidbody.velocity = new Vector2(horizontal, vertical) * speed;

    if (horizontal > 0 && !isFacingRight || horizontal < 0 && isFacingRight) {
      transform.localScale = new Vector2(transform.localScale.x, -transform.localScale.y);
      isFacingRight = !isFacingRight;
    }

    // Lock camera x to player. Camera y is constant.
    Camera.main.transform.position = new Vector3(transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);
    
    // Cycle through background.
    float x = Mathf.Repeat(transform.position.x * 0.01f, 1);
    Vector2 offset = new Vector2(x, baseOffset.y);
    backgroundRenderer.sharedMaterial.SetTextureOffset("_MainTex", offset);
  }

  private GameObject planetInTow;
  void OnTriggerEnter2D(Collider2D collider) {
    if (collider.gameObject.CompareTag("Planet") && planetInTow == null) {
      planetInTow = collider.gameObject;

      DistanceJoint2D joint = planetInTow.AddComponent<DistanceJoint2D>();
      joint.connectedBody = rigidbody;
      joint.anchor = new Vector2(0, 0.5f);
      joint.connectedAnchor = new Vector2(0, -0.22f);

      planetInTow.GetComponent<Rigidbody2D>().isKinematic = false;

      GameObject satellite = planetInTow.FindChildWithTag("Satellite");
      satellite.GetComponent<SatelliteController>().Detach();
    }
  }
}
