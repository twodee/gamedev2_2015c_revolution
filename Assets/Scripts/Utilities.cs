﻿using UnityEngine;
using System.Collections;

public static class Utilities {

  public static GameObject FindChildWithTag(this GameObject parent,
                                            string tag) {

    foreach (Transform xform in parent.transform) {
      if (xform.tag == tag) {
        return xform.gameObject;
      }
    }

    return null;
  }
}
