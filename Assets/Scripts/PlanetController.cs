﻿using UnityEngine;
using System.Collections;

public class PlanetController : MonoBehaviour {
  void OnTriggerEnter2D(Collider2D collider) {
    if (collider.gameObject.CompareTag("Planet")) {
      GameObject satellite = this.gameObject.FindChildWithTag("Satellite");
      if (satellite != null) {
        satellite.GetComponent<SatelliteController>().Detach();    
      }

      Destroy(gameObject);
      /* Destroy(collider.gameObject); */
    }
  }
}
