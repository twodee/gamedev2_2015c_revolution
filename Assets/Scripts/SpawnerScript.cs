﻿using UnityEngine;
using System.Collections;

public class SpawnerScript : MonoBehaviour {

  public GameObject prefab;
  public int numPlanets;
  public float maxDistance;

	void Start () 
  {
    float x = 4f;

	  for(int i = 0; i < numPlanets; i++)
    {

      float xOffset = Random.Range(10f, maxDistance + 10f);
      float y = Random.Range(-3f, 3f);
      x += xOffset;
      GameObject obj = Instantiate(prefab, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
    }  
	}
	
	void Update () 
  {
	  
	}
}
