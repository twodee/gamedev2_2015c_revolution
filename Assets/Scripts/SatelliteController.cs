﻿using UnityEngine;
using System.Collections;

public class SatelliteController : MonoBehaviour {
  void Start() {
  }
  
  void Update() {
  }

  public void Detach() {
    Destroy(GetComponent<HingeJoint2D>());
    GetComponent<Rigidbody2D>().isKinematic = true;
    transform.parent = null;
  }
}
